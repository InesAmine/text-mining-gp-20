---
title: "Descriptif des données"
output: html_notebook
---

bin.dtm2.rda est la matrice dtm2 à laquelle on a ajouté une colonne correspondant à la variable binaire bin.sommeil. Bin.sommeil a deux modalités : 0 si il y a eu un changement au niveau du sommeil, 1 si il n'y en a pas eu. 

dodo.rda est la base de données des questions sur le sommeil. Elle est de type corpus.
Elle peut être utilisée pour faire des nuages de mot avec wordcloud.

dtm.rda est la version documentTermMatrix de dodo.rda. Elle peut être utilisée pour regarder la fréquence des mots etc. 

dtm2.rda est la version matricielle de dtm. Je l'utilise pour créer un vecteur qui sera la variable qualitative en 4 modalités correspondant à la question du sommeil. 

DTM_bigram ??

dtm_bigram_tm ??


DTM_trigram ??

dtm_trigram_tm ??

frequence_bigram contient les bigrammes de la base ainsi que leur fréquence.

frequence_trigram contient les trigrammes de la base ainsi que leur fréquence.

mat_variable_quali.rda contient la matrice dtm2 ainsi que la variable qualitative associée aux données textuelles. Elle peut être utilisée pour faire les graphiques de fréquences et doit être remaniée pour faire de l'apprentissage supervisé sur les données manquantes (les 0)