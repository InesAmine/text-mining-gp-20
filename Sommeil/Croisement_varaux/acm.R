library(FactoMineR)
load(file="Sommeil/donnees_propres/table_vglm.rda")

#on passe les éléments en facteur
table_vglm[,c(1:8)] <- lapply(table_vglm[,c(1:8)] , factor)
table_acm <- table_vglm

################         ACM AVEC TOUTES LES OBSERVATIONS        ########################
###################          ppc et sommeilconfi en sup     #############################

res.mca <- MCA(table_acm,  # Variables quantitatives supplémentaires
               quali.sup = c(5,8),  # Variables qualitatives supplémentaires
               graph=FALSE)

eig.val <- res.mca$eig
barplot(eig.val[, 2], 
        names.arg = 1:nrow(eig.val), 
        main = "Variances Explained by Dimensions (%)",
        xlab = "Principal Dimensions",
        ylab = "Percentage of variances",
        col ="steelblue")
# Add connected line segments to the plot
lines(x = 1:nrow(eig.val), eig.val[, 2], 
      type = "b", pch = 19, col = "red")

# on peut prendre 3 dimensions, après ça fait vraiment un plateau. 

plot.MCA(res.mca, cex=0.7)

# Il y a bcp d'observations : on voit quand même 4 groupes :
#on essaye de faire avec moins d obs en mettant des indivdus en sup pour voir si ça rend mieux

# Résultats des variables actives
res.var <- res.mca$var
res.var$coord          # Coordonnées
res.var$contrib        # Contributions 
res.var$cos2           # Qualité de représentation 
# Résultats des variables quantitatives supplémentaires
res.mca$quali.sup
# Résultats des individus actifs
res.ind <- res.mca$var
res.ind$coord          # Coordonnées
res.ind$contrib        # Contributions 
res.ind$cos2           # Qualités de représentation



################         ACM AVEC la moitié des obs        ########################
###################          ppc et sommeilconfi en sup     #############################

set.seed(3107)
indices <- sample(1:nrow(table_acm), nrow(table_acm)/2)
res.mca2 <- MCA(table_acm, 
               quali.sup = c(5,8),# Variables qualitatives supplémentaires
               ind.sup = indices,
               graph=FALSE)

eig.val2 <- res.mca2$eig #on a quasiment les mêmes valeurs
barplot(eig.val2[, 2], 
        names.arg = 1:nrow(eig.val2), 
        main = "Variances Explained by Dimensions (%), acm moitié obs",
        xlab = "Principal Dimensions",
        ylab = "Percentage of variances",
        col ="steelblue")
# Add connected line segments to the plot
lines(x = 1:nrow(eig.val2), eig.val2[, 2], 
      type = "b", pch = 19, col = "red")

# on peut prendre 3 dimensions, après ça fait vraiment un plateau. 

plot.MCA(res.mca2, invisible = c("ind.sup"), cex=0.7) # on distingue encore 4 groupes

################         ACM AVEC le quart des obs       ########################
###################          ppc et sommeilconfi en sup     #############################

set.seed(3107)
indices_quart <- sample(1:nrow(table_acm), 2830)
res.mca3 <- MCA(table_acm, 
                quali.sup = c(5,8),# Variables qualitatives supplémentaires
                ind.sup = indices_quart,
                graph=FALSE)

eig.val3 <- res.mca3$eig #on a quasiment les mêmes valeurs
barplot(eig.val3[, 2], 
        names.arg = 1:nrow(eig.val3), 
        main = "Variances Explained by Dimensions (%), acm moitié obs",
        xlab = "Principal Dimensions",
        ylab = "Percentage of variances",
        col ="steelblue")
# Add connected line segments to the plot
lines(x = 1:nrow(eig.val3), eig.val3[, 2], 
      type = "b", pch = 19, col = "red")

# on peut prendre 3 dimensions, après ça fait vraiment un plateau. 

plot.MCA(res.mca3, invisible = c("ind.sup"), cex=0.7) # on distingue encore 4 groupes

################         ACM AVEC TOUTES LES OBSERVATIONS        ########################
#######    variables : sexe, age, sieste, intensite apnée  #############

res.mca4 <- MCA(table_acm,  # Variables quantitatives supplémentaires
               quali.sup = c(4,5,7,8),  # Variables qualitatives supplémentaires
               graph=FALSE)

eig.val4 <- res.mca4$eig
barplot(eig.val4[, 2], 
        names.arg = 1:nrow(eig.val4), 
        main = "Variances Explained by Dimensions (%)",
        xlab = "Principal Dimensions",
        ylab = "Percentage of variances",
        col ="steelblue")
# Add connected line segments to the plot
lines(x = 1:nrow(eig.val4), eig.val4[, 2], 
      type = "b", pch = 19, col = "red")

# on peut prendre é dimensions, après ça fait vraiment un plateau. 

plot.MCA(res.mca4, cex=0.8)

# Il y a bcp d'observations : on voit quand même 4 groupes :
#on essaye de faire avec moins d obs en mettant des indivdus en sup pour voir si ça rend mieux

# Résultats des variables actives
res.var4 <- res.mca4$var
res.var4$coord          # Coordonnées
res.var4$contrib        # Contributions 
res.var4$cos2           # Qualité de représentation 
# Résultats des variables quantitatives supplémentaires
res.mca4$quali.sup
# Résultats des individus actifs
res.ind4 <- res.mca4$var
res.ind4$coord          # Coordonnées
res.ind4$contrib        # Contributions 
res.ind4$cos2           # Qualités de représentation


